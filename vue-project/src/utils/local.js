// 设置的时候需要将对象转成字符串,也就是序列化
// 设置本地存储
export const setLocal = (key, value) => {
  if (typeof value === 'object') {
    value = JSON.stringify(value)
  }
  localStorage.setItem(key, value);
}

// 获取本地的值 需要转化成对象，也就是反序列化
// 获取本地存储
export const getLocal = (key) => {
  let value = localStorage.getItem(key) || ''
  if (value.includes('[') || value.includes('{')) {
    // 判断获取的值是对象还是数组，是的话就要反序列化
    return JSON.parse(value);
  } else {
    // 不是对象或者数组直接返回值
    return localStorage.getItem(key) || '';
  }
}

//挂载到window上
window.setLocal = setLocal;
window.getLocal = getLocal;