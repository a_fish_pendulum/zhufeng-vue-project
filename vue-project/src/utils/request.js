// 二次封装axios，做项目内部的个性化处理，避免干扰
// 引入config配置文件
import config from '@/config'
// 引入axios
import axios from 'axios'
// 自定义类，用来封装axios
class HttpRequest {
  constructor() {
    // 可以增加实例属性 后台接口的路径  开发模式和生产模式不一样 在config里新建index.js进行配置
    this.baseURL = config.baseURL;//默认地址
    this.timeout = 5000; // 请求超时时间设置
  }
  //创建单独的拦截器
  setInterceptors(instance) {
    // 请求拦截器
    instance.interceptors.request.use(config => {
      // 在发送请求之前做某件事，可以追加token，转换参数等
      config.headers.authorization = 'Bearer ' + getLocal('token')
      return config;
    });
    // 响应拦截器
    instance.interceptors.response.use(res => {
      // 状态码与后端同步就行，可能是status，也可能是其他名字
      if (res.status == 200) {
        // 服务返回的结果都会放到data中
        if (res.data.err === 0) { // 统一处理错误状态码
          return Promise.resolve(res.data);//成功状态
        } else {
          return Promise.reject(res.data.data);//失败状态
        }
      } else {
        return Promise.reject(res.data.data); //后端实现的话,如果失败了会在返回的结果中增加一个data字段 失败的原因
      }
    }, err => {
      // 单独处理其他的状态码异常
      switch (err.response.status) {
        case '401':
          console.log(err);
          break;
        default:
          break;
      }
      return Promise.reject(err);
    });
  }
  mergeOptions(options) {//合并选项
    return { baseURL: this.baseURL, timeout: this.timeout, ...options }
  }
  request(options) {
    const instance = axios.create(); // 创建axios实例
    this.setInterceptors(instance);//创建单独的拦截器
    const opts = this.mergeOptions(options);//合并选项
    return instance(opts)//单独拦截器的配置项
  }
  get(url, config) { //get请求 以字符串的形式传入 路径参数  ?a=1
    return this.request({
      method: 'get',
      url,
      ...config // 参数可以直接展开
    });
  }
  post(url, data) { //post请求 
    return this.request({
      method: 'post',
      url,
      data: data // post要求必须传入data属性
    })
  }
}
export default new HttpRequest;//抛出封装的类