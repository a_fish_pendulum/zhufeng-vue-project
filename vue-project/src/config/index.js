export default {
  // 根据环境区分baseurl地址
  baseURL: process.env.NODE_ENV === 'development' ? 'http://vue.zhufengpeixun.cn' : '/'
}