import Vue from 'vue'
import Vuex from 'vuex'
// 引入根模块文件rootModule
import rootModule from './rootModule'

Vue.use(Vuex)

// 动态加载子模块，正则匹配modules下面的文件
const files = require.context('./modules', false, /\.js$/);
// 将模块循环遍历出来添加到根模块中
files.keys().forEach(key => {
  // 模块对应的内容
  let store = files(key).default;
  let moduleName = key.replace(/^\.\//, '').replace(/\.js$/, '');
  // 动态的添加模块
  let modules = rootModule.modules = (rootModule.modules || {});
  // let modules = rootModule.modules = rootModule.modules ? rootModule.modules : {};
  modules[moduleName] = store;
  // 使用命名空间，解决模块的命名冲突,页面中使用getter、actions、mutations的时候需要加上所属的模块名
  modules[moduleName].namespaced = true;
});
console.log('sss', rootModule)
let store = new Vuex.Store(rootModule);
export default store;

