import * as user from '@/api/user';
import * as types from '../action-types';
import { setLocal, getLocal } from '@/utils/local';//token存储在本地会话中
import router from '@/router'; // 引入router
import per from '@/router/per.js'; // 引入路由配置文件

//路由过滤器，过滤路由的方法
const filterRouter = (authList) => {
  //conosle.log(authList)//后端返回的数据中auth属性是进行权限鉴定的 
  let auths = authList.map(auth => auth.auth);//获取到auths的名称进行筛选
  function filter(routes) {
    return routes.filter(route => {
      if (auths.includes(route.meta.auth)) {//auths匹配上的就会有相对应的菜单
        if (route.children) {//如果有娃儿的话就继续匹配
          route.children = filter(route.children)
        }
        return route;
      }
    })
  }
  return filter(per)
}

export default {
  state: {
    userInfo: {}, // 用户信息
    hasPermission: false, // 代表用户权限
    menuPermission: false,//代表着菜单权限
  },
  // mutation同步更新信息
  mutations: {
    // 设置用户信息
    [types.SET_USER](state, userInfo) {
      state.userInfo = userInfo;
      if (userInfo && userInfo.token) {//如果有token值就存储在本地会话中
        setLocal('token', userInfo.token);
      } else {
        localStorage.clear('token');
      }
    },
    // 设置用户权限
    [types.SET_PERMISSION](state, has) {
      state.hasPermission = has;
    },
    // 设置路由权限
    [types.SET_MENU_PERMISSION](state, has) {
      state.menuPermission = has;
    }
  },
  // actions异步更新信息
  actions: {
    // 设置用户信息
    async [types.SET_USER]({ commit }, { payload, permission }) {
      commit(types.SET_USER, payload);
      commit(types.SET_PERMISSION, permission)
    },
    // 登录接口
    async [types.USER_LOGIN]({ commit, dispatch }, payload) {
      //payload --- 用户信息
      try {
        let result = await user.login(payload);
        dispatch(types.SET_USER, { payload: result.data, permission: true })
      } catch (e) {
        // this.$message('登录失败');
        return Promise.reject(e);
      }
    },
    // 验证用户是否登录
    async [types.USER_VALIDATE]({ dispatch }) {
      // 如果没token 就不用发请求了 肯定没登录
      if (!getLocal('token')) return false;
      try {
        let result = await user.validate();
        // 派发校验结果
        dispatch(types.SET_USER, { payload: result.data, permission: true })
        return true;
      } catch (e) {
        // 派发校验结果
        dispatch(types.SET_USER, { payload: {}, permission: false });
        return false;
      }
    },
    // 验证用户退出登录
    async [types.USER_LOGOUT]({ dispatch }) {
      dispatch(types.SET_USER, { payload: {}, permission: false })
    },
    // 动态添加路由route
    async [types.ADD_ROUTE]({ commit, state }) {
      // 后端返回的用户的权限
      let authList = state.userInfo.authList;
      //console.log(authList,per)//authList代表着后端返回的数据，per是前端设置的路径
      if (authList) { // 通过权限过滤出当前用户的路由--不同的用户有不同的菜单
        let routes = filterRouter(authList);
        // 找到manager路由作为第一级 如/manager/articleManager.vue
        //在路由的配置信息上进行查找 --- manager这一条
        let route = router.options.routes.find(item => item.path === '/manager');
        route.children = routes; // 给manager添加儿子路由
        router.addRoutes([route]); // 动态添加进去
        commit(types.SET_MENU_PERMISSION, true); // 权限设置完毕
      } else {
        commit(types.SET_MENU_PERMISSION, true); // 权限设置完毕
      }
    }
  }
}