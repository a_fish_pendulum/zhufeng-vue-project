import { getSlider } from '../api/public';
import * as types from './action-types';//存放所有方法的类型，为了直观看到哪些方法调用
import WS from '@/utils/websocket'; // 引入ws

const rootModule = {
  // state vuex的基本数据，用来存储变量
  state: {
    sliders: [],
    ws: null, // ws信息 默认为null
    message: '' // 存放msg信息
  },
  // getters  store 的计算属性
  getters: {},
  // mutation 同步更新数据的方法
  mutations: {
    [types.SET_SLIDERS](state, payload) {
      state.sliders = payload;
    },
    [types.CREATE_WEBSOCKET](state, ws) {//存放ws信息
      state.ws = ws
    },
    [types.SET_MESSAGE](state, msg) {//存放msg信息
      state.message = msg
    }
  },
  // actions 异步更新数据的方法
  actions: {
    // 使用async和await做数据处理
    async [types.SET_SLIDERS]({ commit }) {
      //调用getSlider()的api
      let { data } = await getSlider();
      commit(types.SET_SLIDERS, data)
    },
    async [types.CREATE_WEBSOCKET]({ commit }) {
      let ws = new WS(); // new一个ws
      ws.create(); // 连线启动ws
      commit(types.CREATE_WEBSOCKET, ws)
    }
  },
  // 模块化vuex,让每个模块自己独立拥有自己的属性便于管理
  // modules: {}
};
export default rootModule;