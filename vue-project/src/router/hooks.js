import store from '../store';
import * as types from '../store/action-types'

// 在hook中使用meta的备注在路由钩子函数中进行操作
// 登录权限校验
const loginPermission = async function (to, from, next) {
  console.log('to:' + to.path)//绑在router上
  // let r = await store.dispatch(`user/${types.USER_VALIDATE}`);
  // console.log(r)
  let needLogin = to.matched.some(item => item.meta.needLogin);
  let hasPermission = store.state.user.hasPermission;
  // 如果需要登录且没登录
  if (needLogin && !hasPermission) {
    next('/login');
    return;
  }
  // 登录过 访问登录页面 跳转到首页
  if (to.path === '/login' && hasPermission) {
    next('/');
    return;
  }
  // 其他逻辑直接走下去就行
  next();

  // if (!store.state.user.hasPermission) {
  //   // 没登录 但是访问这个页面需要登录
  //   if (needLogin) {
  //     // if (r) {
  //     //   next(); // 需要登录 并且登录过了 继续即可
  //     // } else {
  //     //   next('/login');//需要登录 且没有登陆就返回登录页进行登录
  //     // }
  //     next('/login');//需要登录 且没有登陆就返回登录页进行登录
  //   } else {
  //     next(); // 没登录 也不用权限
  //   }
  // } else {
  //   // 登录过 访问登录页面 跳转到首页 
  //   if (to.path === '/login') {
  //     next('/');
  //   } else {//登录过没有访问登录页面想干什么就干什么
  //     next();
  //   }
  // }
  // next();
}
// 动态添加菜单路由权限
const menuPermisson = async function (to, from, next) {
  // 先看是否登录了
  if (store.state.user.hasPermission) {
    // 添加路由这里需要判断是否添加过路由了 
    if (!store.state.user.menuPermission) {
      // 添加路由完成，但是是属于异步加载，不会立即更新
      store.dispatch(`user/${types.ADD_ROUTE}`);
      //replace 相当于replaceState替换掉并不进入历史记录 把路径添加完全
      // next({ ...to, replace: true }); // 如果是next()时，进入到页面时报404.
      next();
    } else {
      next();
    }
  } else {
    next();
  }
}
// 动态创建websocket
const createWebSockect = async function (to, from, next) {
  // 如果登陆了 但是没有创建websocket
  if (store.state.user.hasPermission && !store.state.ws) {
    //调用创建ws方法
    store.dispatch(`${types.CREATE_WEBSOCKET}`);
    next();
  } else {
    //登录了并且创建ws
    next();
  }
}

export default {
  loginPermission, menuPermisson, createWebSockect
}
