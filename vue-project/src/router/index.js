import Vue from 'vue'
import VueRouter from 'vue-router'
import hooks from './hooks'

// 重写路由的push方法,处理路由跳转报错信息
const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}

Vue.use(VueRouter)

const files = require.context('./', false, /\.router.js$/);
const routes = [];
files.keys().forEach(key => {
  // 把找到的路由文件插入路由当中
  routes.push(...files(key).default)
});
console.log(routes)

// 入口文件
const router = new VueRouter({
  mode: 'history', //作用去掉#，如果线上需要使用这个后台服务器需要提供支持
  base: process.env.BASE_URL, // 获取环境配置文件的url
  routes
})

//注册hooks在index中
Object.values(hooks).forEach(hook => {
  router.beforeEach(hook.bind(router)); // 将this绑定成router
});

export default router
