// 在router上模块上添加具有权限鉴定的meta来进行备注

export default [{
  path: '/',
  component: () => import( /*webpackChunkName:'home'*/ '@/views/Home.vue')//懒加载--按需加载
}, {
  // *表示最后找不到页面才会处理，也就是所有路由最后面
  path: '*',
  component: () => import( /*webpackChunkName:'404'*/ '@/views/404.vue')
}, {
  path: '/manager',
  component: () => import(/*webpackChunkName:'manager'*/'@/views/manager/index.vue'),
  meta: {
    needLogin: true // 需要登录权限
  }
}]