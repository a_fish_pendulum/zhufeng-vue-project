接口数据一般都是在api文件里，新建config文件夹【只负责配置文件】  ---  配置里面有很多模块，模块都需要把接口用在配置文件来保存下来，在其新建user.js文件和public.js文件。在api文件夹里新建user.js文件和public.js文件【只负责接口文件】<br />**获取轮播图的接口：**[**http://vue.zhufengpeixun.cn/public/getSlider**](http://vue.zhufengpeixun.cn/public/getSlider)<br />api/config/public.js文件
```javascript
// 接口抽离的文件
export default {
    getSlider: '/public/getSlider',  // 获取轮播图接口
}
```
api/config/user.js文件
```javascript
export default {//后面实现用户管理方面的内容
    reg: '/user/reg', //用户注册模块
    login:'/user/login',//用户信息和权限登录
    validate:'/user/validate'//校验用户是否登陆过
}
```
api/public.js文件
```javascript
import config from './config/public';
import axios from '@/utils/request'

// 获取轮播图功能
export const getSlider = () => axios.get(config.getSlider);
//调用接口有两种方式
//1.在页面中直接调用
//2.vuex 获取数据 --actions  <=选择
//用第二种的好处：数据是全局的，复用性高，做缓存功能
```
所以需要vuex配置，在store/rootModule.js文件
```javascript
import { getSlider } from '../api/public';
import * as types from './action-types';//存放所有方法的类型，为了直观看到哪些方法调用

//console.log(types)
export default {
    state: {
        sliders: [],
    },
    mutations: {
        [types.SET_SLIDERS](state, payload) {
            state.sliders = payload;
        },
    actions: {
      //调用getSlider()的api
        async [types.SET_SLIDERS]({ commit }) {
            let { data } = await getSlider();
            //console.log(data)
            commit(types.SET_SLIDERS, data)
        }
    }
}
```
在store文件夹里新建action-type.js文件，为了可以直接了解使用哪些方法，顾名思义：存放所有方法类型的文件。<br />store/action-types.js
```javascript
//这里存放所有的方法类型

//轮播图
export const SET_SLIDERS = 'SET_SLIDERS'
```
在views/Home.vue文件
```javascript
<template>
  <div>
    <!-- 渲染轮播图的组件 -->
    <el-carousel :interval="4000" type="card" height="360px">
      <el-carousel-item v-for="item in sliders" :key="item._id" >
        <img :src="item.url" style="width:100%;height:100%" />
      </el-carousel-item>
    </el-carousel>
  </div>
</template>
<script>
import { mapActions, mapState } from "vuex";
import * as types from "../store/action-types";
export default {
  computed: {
    ...mapState(["sliders"])
  },
  methods: {//调用这个轮播的方法
    ...mapActions([types.SET_SLIDERS])
  },
  mounted() {//页面初始化时进行调用
    if (!this.sliders.length) {
      this[types.SET_SLIDERS]();
    }
  }
};
</script>
```
接收到数据之后要在utils/request.js中进行判断
```javascript
//....        
instance.interceptors.response.use(res => {
            if (res.status == 200) {
                // 服务返回的结果都会放到data中
                if(res.data.err === 0){ // 统一处理错误状态码
                    return Promise.resolve(res.data);//成功状态
                }else{
                    return Promise.reject(res.data.data);//失败状态
                }
            } else {
                return Promise.reject(res.data.data); // 我的后端实现的话 如果失败了会在返回的结果中增加一个data字段 失败的原因
            }
        }, err => {
            // 单独处理其他的状态码异常
            switch (err.response.status) {
                case '401':
                    console.log(err);
                    break;
                default:
                    break;
            }
            return Promise.reject(err);
        });
    }
    //....
```
效果图如视频所示：http://img.zhufengpeixun.cn/0604_1.mp4